package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.repository.CollectionRepository;
import by.itstep.collectionsphotos.repository.PhotoHibernateRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;

    @Autowired
    private CollectionRepository collectionRepository;

    public PhotoEntity update(PhotoEntity photo) {
        if (photo == null) {
            throw new RuntimeException("Photo is not found by id {...}");
        }

        photo.setName(photo.getName());

        PhotoEntity updatedPhoto = photoRepository.update(photo);
        // .. sout()
        return updatedPhoto;
    }

    public void deleteById(int id) {
        PhotoEntity photo = photoRepository.findById(id);
        List<CollectionEntity> photoCollections = photo.getCollections();

        for (CollectionEntity collection : photoCollections) {
            collection.getPhotos().remove(photo);
            collectionRepository.update(collection);
        }
        photoRepository.deleteById(id);
    }

}
// Mapstruct
// hibernate.validation
