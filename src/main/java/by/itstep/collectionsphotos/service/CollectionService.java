package by.itstep.collectionsphotos.service;

import org.springframework.stereotype.Service;

@Service
public class CollectionService { // https://codeshare.io/nzXKB1
    // CollectionRepository + UserRepository + constructor

    // Поиск по id
    // * Не забыть что бросаем исключение если не нашли

    // Поиск всех
    // * Исключение НЕ выбрасываем даже если список пустой

    // Создание
    // * Нужно проверить что айдишник в коллекции НЕ указан
    // * Нужно проверить что такой пользователь есть

    // Обновление
    // * Нужно проверить что айдишник в коллекции указан
    // * Нужно проверить что такая коллекция есть
    // * Нужно проверить что такой пользователь есть

    // Удаление по id
    // * Нужно проверить что такая коллекция есть
    // * Нужно удалить ТОЛЬКО коллекцию (фотки не трогать!)

    // ===============

    // 1. Добавить метод в CollectionService
    //  * public void addPhoto(int collectionId, int photoId) { ... }
    //  * public void removePhoto(int collectionId, int photoId) { ... }



}
