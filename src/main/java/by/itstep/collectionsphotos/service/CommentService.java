package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.CommentFullDto;
import by.itstep.collectionsphotos.dto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentMapper mapper;

    public CommentFullDto findById(Integer id) {
        // ...

        CommentEntity entity = commentRepository.findById(id);
        CommentFullDto dto = mapper.map(entity);
        return dto;
    }

    public List<CommentFullDto> findAll() {
        List<CommentEntity> all = commentRepository.findAll();
        List<CommentFullDto> dtos = mapper.map(all);
        return dtos;
    }

    public CommentFullDto create(CommentCreateDto createRequest) {
        CommentEntity entity = mapper.map(createRequest);

        CommentEntity comment = commentRepository.create(entity);

        CommentFullDto dto = mapper.map(comment);
        return dto;
    }

    public CommentFullDto update(CommentUpdateDto updateRequest) {
        CommentEntity entityToUpdate = commentRepository
                .findById(updateRequest.getId());

        entityToUpdate.setMessage(updateRequest.getMessage());

        CommentEntity comment = commentRepository.update(entityToUpdate);

        CommentFullDto dto = mapper.map(comment);
        return dto;
    }

    public void delete(Integer id) { }

}
