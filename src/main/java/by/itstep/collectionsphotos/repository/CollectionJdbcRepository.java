package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository("jdbc")
public class CollectionJdbcRepository implements CollectionRepository {

    @Override
    public CollectionEntity findById(int id) {
        // ...
        return null;
    }

    @Override
    public List<CollectionEntity> findAll() {
        // ...
        return null;
    }

    @Override
    public CollectionEntity create(CollectionEntity entity) {
        // ...
        return null;
    }

    @Override
    public CollectionEntity update(CollectionEntity entity) {
        // ...
        return null;
    }

    @Override
    public void deleteById(int id) {
        // ...
    }

    @Override
    public void deleteAll() {
        // ...
    }
}
