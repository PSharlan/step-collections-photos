package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Primary
@Repository("hibernate")
public class CollectionHibernateRepository implements CollectionRepository {

    @Override
    public CollectionEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        CollectionEntity foundCollection = em.find(CollectionEntity.class, id);

        em.getTransaction().commit();
        em.close();
        return foundCollection;
    }

    @Override
    public List<CollectionEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<CollectionEntity> allCollections = em
                .createNativeQuery("SELECT * FROM collections", CollectionEntity.class)
                .getResultList();

        em.getTransaction().commit();
        em.close();
        return allCollections;
    }

    @Override
    public CollectionEntity create(CollectionEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public CollectionEntity update(CollectionEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(entity);

        em.getTransaction().commit();
        em.close();
        return entity;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        CollectionEntity entityToRemove = em.find(CollectionEntity.class, id);
        em.remove(entityToRemove);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collections")
                .executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
