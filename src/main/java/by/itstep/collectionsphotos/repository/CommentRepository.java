package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CommentEntity;

import java.util.List;

public interface CommentRepository {

    CommentEntity findById(int id);

    List<CommentEntity> findAll();

    CommentEntity create(CommentEntity entity);

    CommentEntity update(CommentEntity entity);

    void deleteById(int id);

    void deleteAll();

}
