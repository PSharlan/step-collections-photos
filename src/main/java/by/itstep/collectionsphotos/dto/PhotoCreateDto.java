package by.itstep.collectionsphotos.dto;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import lombok.Data;

@Data
public class PhotoCreateDto {

    private String link;
    private String name;

}
