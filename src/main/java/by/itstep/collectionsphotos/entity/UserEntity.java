package by.itstep.collectionsphotos.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "name")
    private String name;

    @ToString.Exclude          // Чтобы небыло цикличных зависимостей
    @EqualsAndHashCode.Exclude // Чтобы небыло цикличных зависимостей
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<CollectionEntity> collections = new ArrayList<>();

    @ToString.Exclude          // Чтобы небыло цикличных зависимостей
    @EqualsAndHashCode.Exclude // Чтобы небыло цикличных зависимостей
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<CommentEntity> comments = new ArrayList<>();

}











